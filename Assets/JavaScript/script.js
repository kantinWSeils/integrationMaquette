$(document).ready(function () {

    /* -_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_- */
    /* Navbar Color */
    $('#contact-nav').css('color', '#009970')

    // Adding color to the different text section of the navbar on hover of the different page section
    $('#home-start').on({
        // When mouse enter the home section it change color for green
        mouseenter: function () {
            $('#home-nav').css('color', '#009970')
        },
        // When mouse leave the home section color is back to normal color
        mouseleave: function () {
            $('#home-nav').css('color', '')
        }
    });
    $('#about-start').on({
        mouseenter: function () {
            $('#about-nav').css('color', '#009970')
        },
        mouseleave: function () {
            $('#about-nav').css('color', '')
        }
    });
    $('#services-start').on({
        mouseenter: function () {
            $('#services-nav').css('color', '#009970')
        },
        mouseleave: function () {
            $('#services-nav').css('color', '')
        }
    });
    $('#portfolio-start').on({
        mouseenter: function () {
            $('#portfolio-nav').css('color', '#009970')
        },
        mouseleave: function () {
            $('#portfolio-nav').css('color', '')
        }
    });
    $('#team-start').on({
        mouseenter: function () {
            $('#team-nav').css('color', '#009970')
        },
        mouseleave: function () {
            $('#team-nav').css('color', '')
        }
    });


    /* -_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_- */
    /* Responsive JQuery */
    // Header changing color according to the device width
    // Change color of the header in small and medium device
    function headerSectionResponsive () {
        // Small/Medium device
        if (window.matchMedia('(max-width: 992px)').matches) {
            $('.bg-color').addClass('bg-success').removeClass('bg-light')
            $('.nav-link').addClass('text-white ms-3')
            $('.btn-margin').addClass('ms-2').removeClass('ms-4')
        }
    }

    // Call to action changing p margin on medium and smaller device
    function callToActionSectionResponsive () {
        // Medium device
        if (window.matchMedia('(max-width: 992px)').matches) {
            $('.p-margin-call-to-action').addClass('ms-3 me-3')
        }
        // Small device
        if (window.matchMedia('(max-width: 576px)').matches) {
            $('.bg-parallax-call-to-action').css('height', '').css('height', '275px')
        }
    }

    // About section changing p margin on medium and smaller device
    function aboutSectionResponsive () {
        // Small/Medium device
        if (window.matchMedia('(max-width: 992px)').matches) {
            $('.about-margin').removeClass('ms-5')
        }
    }

    // Testimonials section changing padding on medium and smaller device
    function testimonialsSectionResponsive () {
        // Small/Medium device
        if (window.matchMedia('(max-width: 992px)').matches) {
            $('.testimonials-padding').addClass('p-0')
        }
    }

    // Team section changing padding on card on medium and smaller device
    function teamSectionResponsive () {
        // Small/Medium device
        if (window.matchMedia('(max-width: 992px)').matches) {
            $('.card-padding-team').addClass('p-0')
            $('.card-container').addClass('p-4').removeClass('p-5')
        }
    }

    // Footer section changing marging on medium device and align all the items in the center on small device
    function footerSectionResponsive () {
        // Medium device
        if (window.matchMedia('(max-width: 992px)').matches) {
            $('.footer-margin-text').addClass('ms-4')
            $('.footer-margin-fa').addClass('me-4')
        }
        // Small device
        if (window.matchMedia('(max-width: 576px)').matches) {
            $('.footer-margin-text').addClass('text-center mt-2').removeClass('ms-4')
            $('.footer-margin-fa').addClass('justify-content-center mt-2 mb-3').removeClass('justify-content-end me-4')
        }
    }


    headerSectionResponsive()
    callToActionSectionResponsive()
    aboutSectionResponsive()
    testimonialsSectionResponsive()
    teamSectionResponsive()
    footerSectionResponsive()


    /* -_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_- */
    /* Button Back To Top */
    // Add a button to go back to the top of the page on scroll 
    // it stay fixed on the bottom right corner of the page
    // It also disapear when back to top
    $('#btn-scroll-top').hide();
    $(window).scroll(function () {
        let height = $(window).scrollTop();
    
        if (height < 100) {
            $('#btn-scroll-top').hide()
        }
        if(height  > 100) {
            $('#btn-scroll-top').show()
        }
    });


    /* -_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_- */
    /* Client On Hover Show Color */
    // Set all the image to be black and on hover, image take back their color (can be done in css simply)
    $('.client-img').css('filter', 'brightness(0)')

    // Client 1 Image hover effect
    $(".client-1").on({
        // Set the brightness to 1 so the color of the image are show when the mouse is above
        mouseenter: function () {
            $(this).css('filter', 'brightness(1)')
        },
        // Set the brightness to "default" so the image is black when the mouse leave the hover
        mouseleave: function () {
            $(this).css('filter', 'brightness(0)')
        }
    });

    // Client 2 Image hover effect
    $(".client-2").on({
        mouseenter: function () {
            $(this).css('filter', 'brightness(1)')
        },
        mouseleave: function () {
            $(this).css('filter', 'brightness(0)')
        }
    });

    // Client 3 Image hover effect
    $(".client-3").on({
        mouseenter: function () {
            $(this).css('filter', 'brightness(1)')
        },
        mouseleave: function () {
            $(this).css('filter', 'brightness(0)')
        }
    });

    // Client 4 Image hover effect
    $(".client-4").on({
        mouseenter: function () {
            $(this).css('filter', 'brightness(1)')
        },
        mouseleave: function () {
            $(this).css('filter', 'brightness(0)')
        }
    });

    // Client 5 Image hover effect
    $(".client-5").on({
        mouseenter: function () {
            $(this).css('filter', 'brightness(1)')
        },
        mouseleave: function () {
            $(this).css('filter', 'brightness(0)')
        }
    });

    // Client 6 Image hover effect
    $(".client-6").on({
        mouseenter: function () {
            $(this).css('filter', 'brightness(1)')
        },
        mouseleave: function () {
            $(this).css('filter', 'brightness(0)')
        }
    });


        /* -_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_ */
    /* Increment Animation */
    // Speed for the different number
    let speed1 = 29;
    let speed2 = 10;
    let speed3 = 0;
    let speed4 = 505;

    /* Call this function with a string containing the ID name to
    * the element containing the number you want to do a count animation on.*/
    function incElementNbr1(id) {
        element = document.getElementById(id);
        endNbr = Number(document.getElementById(id).innerHTML);
        incNbrRec1(0, endNbr, element);
    }

    /*A recursive function to increase the number.*/
    function incNbrRec1(i, endNbr, element) {
        if (i <= endNbr) {
            element.innerHTML = i;
            setTimeout(function() {//Delay a bit before calling the function again.
            incNbrRec1(i + 1, endNbr, element);
            }, speed1);
        }
    }

    // Number 2
    function incElementNbr2(id) {
        element = document.getElementById(id);
        endNbr = Number(document.getElementById(id).innerHTML);
        incNbrRec2(0, endNbr, element);
    }

    /*A recursive function to increase the number.*/
    function incNbrRec2(i, endNbr, element) {
        if (i <= endNbr) {
            element.innerHTML = i;
            setTimeout(function() {//Delay a bit before calling the function again.
            incNbrRec2(i + 1, endNbr, element);
            }, speed2);
        }
    }

    // Number 3
    function incElementNbr3(id) {
        element = document.getElementById(id);
        endNbr = Number(document.getElementById(id).innerHTML);
        incNbrRec3(0, endNbr, element);
    }

    /*A recursive function to increase the number.*/
    function incNbrRec3(i, endNbr, element) {
        if (i <= endNbr) {
            element.innerHTML = i;
            setTimeout(function() {//Delay a bit before calling the function again.
            incNbrRec3(i + 1, endNbr, element);
            }, speed3);
        }
    }

    // Number 4
    function incElementNbr4(id) {
        element = document.getElementById(id);
        endNbr = Number(document.getElementById(id).innerHTML);
        incNbrRec4(0, endNbr, element);
    }

    /*A recursive function to increase the number.*/
    function incNbrRec4(i, endNbr, element) {
        if (i <= endNbr) {
            element.innerHTML = i;
            setTimeout(function() {//Delay a bit before calling the function again.
            incNbrRec4(i + 1, endNbr, element);
            }, speed4);
        }
    }

    incElementNbr1("number1"); /*Call this funtion with the ID-name for that element to increase the number within*/
    incElementNbr2("number2");
    incElementNbr3("number3");
    incElementNbr4("number4");


    /* -_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_- */
    /* OWL Carousel JQuery */
    // Owl Carousel initialisation and option
    $('.owl-carousel').owlCarousel({
        loop:true,
        autoplay:true,
        autoplayTimeout:3000,
        autoplayHoverPause:true,
        margin:10,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
            },
            600:{
                items:1,
            },
            1000:{
                items:2,
                dotsEach:1,
            }
        }
    })


    /* -_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_- */
    /* Isotope JQuery */
    // initialisation of Isotope
    let $grid = $('.grid').isotope({
    // options
        itemSelector: '.grid-item',
    });
    // filter items on button click
    $('.filter-button-group').on( 'click', 'button', function() {
        let filterValue = $(this).attr('data-filter');
        $grid.isotope({ filter: filterValue });
    });


    /* -_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_- */
    /* Change color of the filter button on click for the portfolio */
    // On hover, filter button get the green color 
    // All filter button is by default green color
    // On click of a filter button, change it colors to green and other button go back to normal color

    $('#btn-portfolio-all').on("click", function () {
        // Filter button "All" take green color ...
        $('#btn-portfolio-all').addClass('btn-secondary').removeClass('btn-outline-secondary');
        // ... And other filter button take normal color
        $('#btn-portfolio-app').addClass('btn-outline-secondary').removeClass('btn-secondary');
        $('#btn-portfolio-card').addClass('btn-outline-secondary').removeClass('btn-secondary');
        $('#btn-portfolio-web').addClass('btn-outline-secondary').removeClass('btn-secondary');
    })

    $('#btn-portfolio-app').on("click", function () {
        $('#btn-portfolio-app').addClass('btn-secondary').removeClass('btn-outline-secondary');
        $('#btn-portfolio-all').addClass('btn-outline-secondary').removeClass('btn-secondary');
        $('#btn-portfolio-card').addClass('btn-outline-secondary').removeClass('btn-secondary');
        $('#btn-portfolio-web').addClass('btn-outline-secondary').removeClass('btn-secondary');
    })

    $('#btn-portfolio-card').on("click", function () {
        $('#btn-portfolio-card').addClass('btn-secondary').removeClass('btn-outline-secondary');
        $('#btn-portfolio-all').addClass('btn-outline-secondary').removeClass('btn-secondary');
        $('#btn-portfolio-app').addClass('btn-outline-secondary').removeClass('btn-secondary');
        $('#btn-portfolio-web').addClass('btn-outline-secondary').removeClass('btn-secondary');
    })

    $('#btn-portfolio-web').on("click", function () {
        $('#btn-portfolio-web').addClass('btn-secondary').removeClass('btn-outline-secondary');
        $('#btn-portfolio-all').addClass('btn-outline-secondary').removeClass('btn-secondary');
        $('#btn-portfolio-app').addClass('btn-outline-secondary').removeClass('btn-secondary');
        $('#btn-portfolio-card').addClass('btn-outline-secondary').removeClass('btn-secondary');
    })

});